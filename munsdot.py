#!/usr/bin/python
import glob, os, configparser, func, sys, shutil
from pprint import pprint
filesFolder = "dotfiles/"
backupFolder = "oldfiles/"
configFile = "files.conf"

conf = func.setConfig(configFile)

fileList=glob.glob(filesFolder+"*")+glob.glob(filesFolder+".*")
for filePath in fileList:
  target=func.fileCheck(filePath)
  fullPath = os.path.dirname(os.path.realpath(__file__))+"/"+filePath
  fullBackupPath = os.path.dirname(os.path.realpath(__file__))+"/"+backupFolder
#  print(filePath)
  if target:
    if func.whatIsThis(target):
      try:
        if func.whatIsThis(target) == "l":
          if func.ynmIn(target + " is a symlink, delete to replace?(Yes, No, Maybe)", True):
            print("deleting " + target)
            os.unlink(target)
            print(target+" should be removed, here's a check: " + str(func.whatIsThis(target)))
        else:
          print(target + " exists and is a " + func.witLib(func.whatIsThis(target)) + ". So we'll move it to" + fullBackupPath)
          os.rename(target,fullBackupPath+os.path.basename(target))
      except Exception, e:
        print("ERROR: moving '"+target+"' to '"+fullBackupPath+os.path.basename(target)+"'")
        sys.exit(str(e))
    print("os.symlink("+fullPath+","+target+")")
    try:
      os.symlink(fullPath,target)
    except Exception, e:
      print("ERROR: creating symlink " + fullPath + " at " + target)
      sys.exit(str(e))
