dotfiles by munsking.

all code under GPL3 or later

EARLY ALPHA (like all the other stuff i make lol)

For each file in the /dotfiles/ folder it checks if there's an entry with the same name in files.dot, if an entry is found, it checks if the target location is already in use, if so it moves that file to /oldfiles/ and then it creates a symlink in that place to the file in the dotfiles directory.

This means you can use a git repo or something else to store your dotfiles and easily re-install them on a new install (probably only works on linux for now)
