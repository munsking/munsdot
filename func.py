#!/usr/bin/python
import glob, os, configparser, sys

config = 0
confPath = ""

def whatIsThis(file):
  p = os.path
  if p.isfile(file):
    return "f"
  elif p.islink(file):
    return "l"
  elif p.isdir(file):
    return "d"
  else:
    return False
    
def witLib(key):
  return {
    "f" : "file",
    "l" : "symlink",
    "p" : "full path",
    "d" : "folder",
    False : "error"
  }.get(key,0)

def fileCheck(filePath):
  file = os.path.basename(filePath)
#  pprint(configData)
  if config.has_option("dotfiles",file):
#    print(file + " is equal to " + config.get("dotfiles",file))
    return os.path.expanduser(config.get("dotfiles",file))
  else:
    if ynmIn(file + " is not in your config file, skip? (Yes, No, Maybe)",True):
      print("skipping " + file)
      return False
    else:
      print("Edit the config file and press any key to reload the config file.")
      singleIn()
      global confPath
      setConfig(confPath)
      fileCheck(filePath)
#  print(file + " is a " + witLib(whatIsThis(filePath)))
  
def setConfig(confP):
  global config
  global confPath
  confPath = confP
  config = configparser.ConfigParser()
  config.read(confP)
  return config
  
def ynmIn(msg,default):
  print(msg)
  
  ret = singleIn().lower()
  if ret == "y":
    return True
  elif ret == "n":
    return False
  elif ret == "m":
    print("I'll repeat the question.")
    ynmIn(msg,default)
  else:
    print("invalid input, using default value: " + str(default))
    return default

def singleIn():
  try:
      # Win32
      from msvcrt import getch
  except ImportError:
      # UNIX
      def getch():
          import sys, tty, termios
          fd = sys.stdin.fileno()
          old = termios.tcgetattr(fd)
          try:
              tty.setraw(fd)
              return sys.stdin.read(1)
          finally:
              termios.tcsetattr(fd, termios.TCSADRAIN, old)
  return getch();
